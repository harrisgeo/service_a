<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Show_accounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'show_accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shows all existing accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $names = '';
        foreach(Account::all_names() as $name)
        {
            $names .= $name . ', ';
        }


        echo 'Available accounts: ' . substr($names, 0, -2);
        echo "\n";
    }
}
