<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class view_balance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view_balance {account_name=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shows the balance of the default account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('account_name') ?? 'default';
        $account = Account::where('name', $name)->get();
        echo count($account) ? $account[0]->amount : 'Account ' . $name .' doesn\'t exist';
        echo "\n";

    }
}
