<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class close_account extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close_account {account_name=NULL}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '{account_name} Close account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('account_name');
        
        if ($name != 'NULL' && $name != '')
        {
            $account = Account::where('name',$name)->first();
            if (count($account)) // valid account
            {
                $check = Account::can_close($account->id);
                if ($check['result']) // can be deleted
                {
                    $account->delete();
                    echo $name . ' account has been closed';
                }
                else // cannot be deleted
                {
                    echo $check['reason'];
                }
            }
            else
            {
                echo 'Account ' . $name . ' doesn\'t exist';
            }
            // echo $name;
            
        }
        else
        {
            echo 'Please enter a valid account name';
        }
        echo "\n";
    }
}
