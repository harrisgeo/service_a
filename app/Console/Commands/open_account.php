<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Open_account extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'open_account {account_name=NULL}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '{account_name} Open new account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('account_name');
        if ($name != 'NULL' && $name != '') // valid name
        {
            $account = Account::where('name',$name)->first();

            if (!count($account)) // no other account with the same name
            {
                $new_account = new Account;
                $new_account->name = $name;
                $new_account->amount = 0;
                $new_account->locked = 0;
                $new_account->overdraft = 0;

                $new_account->save();

                echo "Account " . $new_account->name . ' created successfully';
            }
            else // account name already exists
            {
                echo 'Name ' . $name . ' is already in use. Please select a new name';
            }
        }
        else // name invalid
        {
            echo 'Please provide a name for your new account';
        }

        echo "\n";
    }
}
