<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account as Account;

class Apply_overdraft extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apply_overdraft {account_name=default} {overdraft=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '{account_name} {overdraft} Apply for an overdraft. If approved then you can withdraw up to overdraft amount';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('account_name');
        $overdraft = (int) $this->argument('overdraft');

        $account = Account::by_name($name);

        if ($account)
        {
            if (Account::apply_overdraft($account->id, $overdraft)) // overdraft approved
            {
                echo 'The requested overdraft has been approved. Your maximum negative balance now is: ' . $overdraft;
            }
            else // overdraft rejected
            {
                echo 'The requested overdraft has been rejected. Please check your application';
            }
        }
        else
        {
            echo "Account doesn't exist";
        }

        echo "\n";
    }
}
