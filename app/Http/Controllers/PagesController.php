<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function coupon($brand = '', $value = 0, $limit = 0)
    {
    	// return view('coupon', ['brand' => $brand, 'value' => $value, 'limit' => $limit]);
        $url = 'http://localhost:3000/get-coupon';

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec ($ch);
        $info = curl_getinfo($ch);
        $http_result = $info ['http_code'];
        curl_close($ch);

        // $res = json_decode($output);
        print_r(json_decode($output)->brand);
        // return $output;
        // return json_decode($output);
        // return response()->json([
    		// 'brand' => $brand,
    		// 'value' => $value,
    		// 'limit' => $limit,
     //        'res' => $output,
    	// ]);
    }

    public function get_coupon()
    {
      $get = $_GET;
      return redirect('/coupon/' . $get['brand'] . '/' . $get['value'] . '/' . $get['limit']);
    }
}
