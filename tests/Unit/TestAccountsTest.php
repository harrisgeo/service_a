<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestAccountsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample($command = '')
    {
        $this->open_account('open_account');
        $this->show_accounts('show_accounts');
        $this->balance('balance');
        $this->apply_overdraft('apply_overdraft');
        $this->withdraw('withdraw');
        $this->deposit('deposit');
        $this->close_account('close_account');

        $this->remove_test_data();
    }

    /**
    * Tests for open_account command
    * @param $command = string
    */
    private function open_account($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['account_name' => 'foo']);
            $this->artisan($command, ['account_name' => 'bar']);
            $this->artisan($command, ['account_name' => 'test']);
            $this->artisan($command, ['account_name' => '']);
            $this->artisan($command, ['account_name' => 'default']);
            echo "\n";
            $this->assertTrue(true);
        }
    }
    
    /**
    * Tests for show_accounts command
    * @param $command = string
    */
    private function show_accounts($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command);
            echo "\n\n";
            $this->assertTrue(true);
        }
    }

    /**
    * Tests for balance command
    * @param $command = string
    */
    private function balance($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['account_name' => 'foo']);
            $this->artisan($command, ['account_name' => '']);
            $this->artisan($command, ['account_name' => 'default']);
            echo "\n";
            $this->assertTrue(true);
        }
    }

    /**
    * Tests for apply_overdraft command
    * @param $command = string
    */
    private function apply_overdraft($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['account_name' => '', 'overdraft' => '']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '50']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '300']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '2000']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '3000']);
            $this->artisan($command, ['account_name' => 'foo', 'overdraft' => '200']);
            echo "\n";
            $this->assertTrue(true);
        }
    }

    /**
    * Tests for withdraw command
    * @param $command = string
    */
    private function withdraw($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['amount' => '', 'account_name' => '']);
            $this->artisan($command, ['amount' => '', 'account_name' => 'test']);
            $this->artisan($command, ['amount' => '', 'account_name' => 'foo']);
            $this->artisan($command, ['amount' => '10', 'account_name' => 'foo']);
            $this->artisan($command, ['amount' => '1990', 'account_name' => 'foo']);
            $this->artisan($command, ['amount' => '10', 'account_name' => 'foo']);
            echo "\n";
            $this->assertTrue(true);    
        }
    }

    /**
    * Tests for deposit command
    * @param $command = string
    */
    private function deposit($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['amount' => '', 'account_name' => '']);
            $this->artisan($command, ['amount' => '', 'account_name' => 'bar']);
            $this->artisan($command, ['amount' => '100', 'account_name' => 'bar']);
            echo "\n";
            $this->assertTrue(true);    
        }     
    }

    /**
    * Tests for close_account command
    * @param $command = string
    */
    private function close_account($command = '')
    {
        if ($command != '')
        {
            echo "\nTests for " . $command . " command\n";
            $this->artisan($command, ['account_name' => '']);
            $this->artisan($command, ['account_name' => 'foo']);
            $this->artisan($command, ['account_name' => 'bar']);
            $this->artisan($command, ['account_name' => 'default']);
            $this->artisan($command, ['account_name' => 'test']);
            echo "\n";
            $this->assertTrue(true);
        }
    }

    /**
    * Remove all unit testing data from the database
    */
    private function remove_test_data()
    {
        echo "\nRemove all unit test data\n";
        $this->artisan('deposit', ['amount' => '2000', 'account_name' => 'foo']);
        $this->artisan('withdraw', ['amount' => '100', 'account_name' => 'bar']);

        $this->artisan('close_account', ['account_name' => 'foo']);
        $this->artisan('close_account', ['account_name' => 'bar']);
        echo "\n";
        $this->assertTrue(true);    
    }

}
